FROM registry.gitlab.com/pauloandre2000_01/consumo-gasolina-devops:dependencies

COPY . $APP_PATH

RUN mkdir -p assets/static \
  && python manage.py collectstatic --noinput

#ENTRYPOINT ['python', '/usr/src/app/manage.py', 'runserver', '0.0.0.0:8000']